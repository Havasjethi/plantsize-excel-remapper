import { Command } from 'commander';
import { existsSync } from 'fs';
import { resolve } from 'path';
import XLSX, { CellObject } from 'xlsx';

const command = new Command()
  .name('Biolog mapper')
  .description('Transponse data, organise it in a structure and calculates average for the given properties')
  .version('1.0.0')
  .argument('<FileName>', 'Input file name')
  .argument('<PlantNames>', 'Plant Names separated with commas eg.: Little_1,Plant-2,Xk5')
  .argument(
    '<Extractable Propertires>',
    'Measured properties which need to be extracted. Separated with commans ConvexArea,Height'
  )
  .option('-o, --out <outFile>', 'Output file (If not provided writes to the input file)')
  .option('-r, --resultSheet <sheet name>', 'Name of the result sheet', '__Result__')
  .option('-cs, -chunk_space <number>', 'Space between chunks', '1')
  .option('-cp, -chunk_padding <number>', 'Chunk padding', '1')
  .action((fileName, rawPlantNames, rawExtractableProperties, options) => {
    const {
      resultSheet,
      Chunk_space: rawChunkSpace,
      Chunk_padding: rawChunkPadding,
      out: outFile,
    } = options;
    const chunkSpace = +rawChunkSpace;
    const chunkPadding = +rawChunkPadding;
    const plantNames = rawPlantNames.split(',');
    const extractableProperties = rawExtractableProperties.split(',');

    const sheetCreator = new SheetCreator();

    try {
      if (isNaN(chunkPadding)) {
        throw new CatchableError(
          `Chunk padding is not a valid number! ${options.Chunk_padding}`
        );
      }
      if (isNaN(chunkSpace)) {
        throw new CatchableError(`Chunk space is not a valid number! ${options.Chunk_padding}`);
      }
      sheetCreator.processShit({
        fileName,
        chunkDistance: chunkSpace,
        chunkRightPadding: chunkPadding,
        dataNames: extractableProperties,
        plantNames,
        resultSheetName: resultSheet,
        resultFilename: outFile ?? false,
      });
    } catch (error: any) {
      if (error.raw_message !== undefined) {
        console.log('\t ::: Input Error :::');
        console.log(error.raw_message);
      } else {
        console.log(error);
        console.log("\n\t ::: We couldn't recover from that, sorry! :::");
      }
    }
  });
interface Options {
  fileName: string;
  plantNames: string[];
  dataNames: string[];
  resultFilename: string | false;
  resultSheetName: string;
  chunkDistance: number;
  chunkRightPadding: number;
}

// const file_name = 'low_light.xls';
const PLANT_ROW = 'Rows';
const SHEET_NAME = '__sheet_name__';

declare global {
  interface Array<T> {
    enumerate(): Array<[number, T]>;
  }
}

Array.prototype.enumerate = function () {
  return this.map((value, index) => [index, value]);
};

type TransedData = {
  [data: string]: { [sheet: string]: { [plant: string]: (number | undefined)[] } };
};

const main = () => {
  command.parse(process.argv);
};

const test_main = () => {
  const plantNames = ['Col-0', 'DUI-9', '828'];
  const dataNames = ['PixelArea', 'ConvexArea'];
  const resultFilename = 'low_light.edited.xls';
  const resultSheetName = '__Result__';
  const chunkDistance = 1;
  const chunkRightPadding = 1;
  const fileName = resolve(__dirname, 'data', 'low_light.xls');

  const options = {
    plantNames,
    dataNames,
    resultFilename,
    resultSheetName,
    chunkDistance,
    chunkRightPadding,
    fileName,
  };
  const sheetCreator = new SheetCreator();
  sheetCreator.processShit(options);
};

type Chunk = ChunkRow[];
type ChunkRow = ChunkCell[];
type ChunkCell = number | undefined | XLSX.CellObject | string;

class CatchableError extends Error {
  raw_message: string;

  constructor(message) {
    super(message);
    this.raw_message = message;
  }

  static fileNotFound(fileName: string) {
    return new CatchableError(`Given filename does not exits: ${fileName}`);
  }

  static propertyNotFound(property: string) {
    return new CatchableError(`Given Property name not found: ${property}`);
  }
}

class SheetCreator {
  processShit(options: Options) {
    const workBook = this.readData(options.fileName);
    const extractedData = this.extractSheetData(workBook);

    const extractedPlantNumbers = Object.keys(extractedData).length;
    const givenPlantNames = options.plantNames.length;

    if (extractedPlantNumbers !== givenPlantNames) {
      throw new CatchableError(
        `Given plant names does not match the extracted amount! \n (Provided: ${givenPlantNames} Required: ${extractedPlantNumbers})`
      );
    }

    const testPlant = extractedData[Object.keys(extractedData)[0]][0] as Object;
    options.dataNames.forEach((reqestedPlantName) => {
      if (!testPlant.hasOwnProperty(reqestedPlantName)) {
        throw new CatchableError(
          `Given Property name not found: ${reqestedPlantName}\n Found This values: ${Object.keys(
            testPlant
          )
            .filter((e) => e !== SHEET_NAME && e !== 'Date')
            .join(',')}`
        );
      }
    });

    const transformedData = this.transformData(extractedData, options.dataNames);
    const sheet = this.createSheetFromTransformed(transformedData, options);

    XLSX.utils.book_append_sheet(workBook, sheet, options.resultSheetName);
    this.save(workBook, options.resultFilename || options.fileName);
  }

  /**
   * A reather ugly function which transforms transformed data and creates the result worksheet
   */
  createSheetFromTransformed(transformed: TransedData, options: Options): XLSX.WorkSheet {
    const { dataNames, chunkRightPadding } = options;
    const firstSheetName = Object.keys(transformed[dataNames[0]])[0];
    const numberOfPlants = Object.keys(transformed[dataNames[0]][firstSheetName]).length;
    const maxChunkSize = numberOfPlants + chunkRightPadding;

    const itemsPerDataMatrices = 2;
    const totalHeaderHeight = 1 + 1; // Header row + Empty line

    const propertyChunks: { [property: string]: Chunk[] } = this.createChunks(
      transformed,
      itemsPerDataMatrices,
      maxChunkSize,
      totalHeaderHeight,
      options
    );
    const totalRows = Math.ceil(
      propertyChunks[Object.keys(propertyChunks)[0]].length / itemsPerDataMatrices
    );

    const resultChunks: ChunkRow[] = [];
    for (let row = 0; row < totalRows; row++) {
      const chunkRow: Chunk[] = [];

      // Create chunk row from correspondig Data matrices
      for (const propertyName of dataNames) {
        const propertyChunk = propertyChunks[propertyName];

        for (let rowColIndex = 0; rowColIndex < itemsPerDataMatrices; rowColIndex++) {
          chunkRow.push(propertyChunk[row * itemsPerDataMatrices + rowColIndex]);
        }
      }

      const chunkLenght = chunkRow[0].length;
      const items = new Array(chunkLenght);

      for (let i = 0; i < chunkLenght; i++) {
        items[i] = ([] as any).concat(...chunkRow.map((e) => e[i]));
        // try {
        //   items[i] = ([] as any).concat(...chunkRow.map(e => e[i]));
        // } catch (e) {
        //   console.log('\n\t !! WARNING !! :: The output might contain some errros!\n')
        //   items[i] = ([] as any).concat(...chunkRow.map(e =>
        //     (e ?? [])[i] ?? undefined
        //   ));
        // }
      }

      resultChunks.push(...items);
    }

    this._createHeaderRow(resultChunks, dataNames, totalHeaderHeight);

    return XLSX.utils.aoa_to_sheet(resultChunks);
  }

  _createHeaderRow(resultChunks: any[], dataNames: string[], totalHeight: number) {
    const total_length = resultChunks[0].length;
    const totalAttribueDistance = total_length / dataNames.length;
    const header = new Array(total_length);
    for (let i = 0; i < header.length; i++) {
      if (i % totalAttribueDistance === 0) {
        header[i] = dataNames[i / totalAttribueDistance];
      }
    }

    let i = 1;
    while (i++ < totalHeight) {
      resultChunks.unshift(new Array(total_length));
    }
    resultChunks.unshift(header);
  }

  createChunks(
    transformed: TransedData,
    matrixPerRow: number,
    paddedChunkWidth: number,
    offset: number,
    { dataNames, chunkDistance }: Options
  ): { [property: string]: Chunk[] } {
    const createdChunks: { [property: string]: Chunk[] } = {};

    for (const [propIndex, property] of dataNames.enumerate()) {
      const _sheets = transformed[property];

      for (const [index, [sheetName, sheetPlantData]] of Object.entries(_sheets).enumerate()) {
        const plantNames = Object.keys(sheetPlantData);
        const chunk: Chunk = [
          [sheetName],
          plantNames.map((e) => (+e === +e ? plantNames[+e - 1] : e)),
        ];

        // Create Chunk
        const plantDataRows = sheetPlantData[plantNames[0]].map((_, index) =>
          plantNames.map((plantKey) => sheetPlantData[plantKey][index])
        );
        chunk.push(...plantDataRows);
        chunk.push([]);

        const height = plantDataRows.length - 1 + chunkDistance + 3;
        // TODO :: Add optional flag!!
        const averageRow = plantNames.map((_name, flowerIndex): CellObject => {
          const colIndex =
            ((index % matrixPerRow) + propIndex * matrixPerRow) * paddedChunkWidth +
            flowerIndex;
          const rowIndex = Math.floor(index / matrixPerRow);

          const startingIndex = offset + height * rowIndex + 2 * (rowIndex + 1);
          const constEndingIndex = startingIndex + plantDataRows.length - 1;

          const starting = XLSX.utils.encode_cell({ c: colIndex, r: startingIndex });
          const ending = XLSX.utils.encode_cell({ c: colIndex, r: constEndingIndex });

          return { t: 'n', f: `AVERAGE(${starting}:${ending})` };
        });
        chunk.push(averageRow);

        // Add Space between Chunks
        for (let i = 0; i < chunkDistance; i++) {
          chunk.push([]);
        }

        this.fillMatrixTo(chunk, paddedChunkWidth);
        (createdChunks[property] ??= []).push(chunk);
      }
    }

    return createdChunks;
  }

  fillMatrixTo<T>(matix: (T | undefined)[][], toExtend: number) {
    for (const chunkRows of matix) {
      const originalSize = chunkRows.length;
      chunkRows.length = toExtend;
      chunkRows.fill(undefined, originalSize);
    }
  }

  extractSheetData(workBook: XLSX.WorkBook) {
    const workSheets = workBook.SheetNames;
    const writeMap = this.getWriteMap(XLSX.utils.sheet_to_json(workBook.Sheets[workSheets[0]]));
    const writeMapEntries = Object.entries(writeMap);

    const datas: { [plant: string]: any[] } = {};

    for (const sheetName of workBook.SheetNames) {
      const sheetData = XLSX.utils.sheet_to_json(workBook.Sheets[sheetName], {
        blankrows: true,
      });
      const plantData = this.extractPlantData(sheetData, writeMapEntries);

      Object.keys(plantData)
        .filter((key) => key !== undefined)
        .forEach((key) => {
          plantData[key][SHEET_NAME] = sheetName;
          (datas[key] ??= []).push(plantData[key]);
        });
    }

    return datas;
  }

  transformData(extractedData, dataNames: Options['dataNames']): TransedData {
    const transformed: TransedData = {};
    for (const rowName of dataNames) {
      transformed[rowName] = {};
    }

    for (const [plantName, plantData] of Object.entries(extractedData)) {
      for (const dataItem of plantData as any[]) {
        const sheetName = dataItem[SHEET_NAME];
        for (const dataName of dataNames) {
          const sheet = (transformed[dataName][sheetName] ??= {});
          sheet[plantName] = dataItem[dataName];
        }
      }
    }

    return transformed;
  }

  getWriteMap(jsonSheetData): { [key: string]: string } {
    const keys = Object.keys(jsonSheetData[0]);
    const write_map: { [key: string]: string } = {};

    for (const key of keys) {
      write_map[key] = key.split('_')[0];
    }

    // This forces the 'A => A' mapping if 'A_1 => A' mapping exists
    Object.values(write_map).forEach((value) => {
      write_map[value] = value;
    });

    return write_map;
  }

  extractPlantData(
    jsonSheetData,
    writeMapEntries: [string, string][]
  ): { [plant: string]: any } {
    const plantData: { [plant: string]: any } = {};

    for (const [key, value] of writeMapEntries) {
      for (const rowData of jsonSheetData) {
        const plantNumber = rowData[PLANT_ROW];

        if (plantNumber === undefined) continue;

        const dataRow = (plantData[plantNumber] ??= {});
        (dataRow[value] ??= []).push(rowData[key]);
      }
    }

    return plantData;
  }

  readData(fileName: string): XLSX.WorkBook {
    if (!existsSync(fileName)) {
      throw new CatchableError(`Given filename does not exits: ${fileName}`);
    }

    return XLSX.readFile(fileName, { WTF: true });
  }

  save(workBook: XLSX.WorkBook, fileName: string) {
    XLSX.writeFile(workBook, fileName, { WTF: true });
  }
}

main();
